<?php

namespace Api\Rest;

use Rest\Controller;
use Data\Params;
use Illuminate\Http\Request;

class TestController extends Controller
{

    protected function get(Request $request, Params $params)
    {
        return [
            'success' => true
        ];
    }

}