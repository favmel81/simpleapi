<?php


require 'vendor/autoload.php';

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Routing\Router;
use Routing\Route;
use Routing\RouteMatcherByPattern;
use Data\Params;


$request = Request::capture();
$response = JsonResponse::create();

$router = new Router();


$matcher = new RouteMatcherByPattern(
    '{?action}/{?id}',
    [
        Request::METHOD_GET,
        Request::METHOD_POST,
        Request::METHOD_PUT,
        Request::METHOD_DELETE
    ]);


$route = new Route($matcher, function (Request $request, Params $params) {


    return (new \Api\Rest\TestController())->run($request, $params);





}, 'api/test');


$router->addRoute($route);


try {
    $result = $router->handleRequest($request);
    if (!$result->isSuccess()) {
        throw new \Exception('Page not found');
    }



    $handler = $result->getRouteHandler();
    $params = $result->getRouteMatcher()->getParams();
    $data = $handler($request, $params);
    $response->setData($data);

} catch (\Throwable $e) {
    $response->setData(['error' => $e->getMessage()])->setStatusCode(400);
}

$response->prepare($request)->send();