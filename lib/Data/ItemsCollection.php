<?php

namespace Data;


class ItemsCollection implements SaveAbleCollection
{

    /**
     * @var Storage
     */
    protected $storage;

    /**
     * @var Model[]
     */
    protected $data = [];

    /**
     * Collection constructor.
     * @param Storage $storage
     */
    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
        $this->load();
    }

    /**
     * @param Model $model
     */
    public function add(Model $model) {
        $this->data[] = $model;
    }

    public function remove(Model $model) {
        $this->removeById($model->getId());
    }

    public function removeById($id) {
        $data = [];
        foreach ($this->data as $model) {
            if ($model->getId() != $id) {
                $data[] = $model;
                continue;
            }
        }

        $this->data = $data;
    }

    /**
     *
     */
    public function clean() {
        $this->data = [];
    }

    /**
     * @param $id
     * @return Model|null
     */
    public function findById($id): Model {
        foreach ($this->data as $model) {
            if ($model->getId() == $id) {
                return $model;
            }
        }

        return null;
    }

    public function getAllAsArray() {
        $data = [];
        foreach ($this->data as $model) {
            $data[] = $model->asArray();
        }

        return $data;
    }

    public function load() {
        $this->data = $this->storage->load();
    }

    public function save() {
        $this->storage->save($this->data);
    }
}