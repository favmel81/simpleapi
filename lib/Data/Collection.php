<?php

namespace Data;

interface Collection
{
    /**
     * @param Model $model
     */
    public function add(Model $model);

    public function remove(Model $model);

    public function removeById($id);

    public function findById($id): Model;

    public function getAllAsArray();
}