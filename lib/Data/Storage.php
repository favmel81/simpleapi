<?php

namespace Data;

interface Storage
{
    /**
     * @param array $data
     * @return mixed
     */
    public function save(array $data);

    /**
     * @return array
     */
    public function load(): array ;
}