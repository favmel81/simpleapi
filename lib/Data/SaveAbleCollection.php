<?php

namespace Data;

interface SaveAbleCollection extends Collection
{
    public function save();
}