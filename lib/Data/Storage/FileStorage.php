<?php

namespace Data\Storage;


use Data\Storage;

class FileStorage implements Storage
{
    /**
     * @var string
     */
    protected $file;

    /**
     * FileStorage constructor.
     * @param string $file
     */
    public function __construct(string $file)
    {
        $this->file = $file;
    }

    /**
     * @param array $data
     */
    public function save(array $data)
    {
        $data = serialize($data);
        file_put_contents($this->file, $data);
    }

    /**
     * @return array
     */
    public function load(): array
    {
        if (!file_exists($this->file)) {
            return [];
        }

        $data = file_get_contents($this->file);
        return unserialize($data);
    }
}