<?php

namespace Data;


class Params implements \Countable
{
    /**
     * @var array
     */
    private $data = [];

    public function __construct(array $data = null)
    {
        if ($data === null) {
            $data = [];
        }

        $this->data = $data;
    }

    /**
     * @param string $name
     * @param null $value
     */
    public function add(string $name, $value = null) {
        $this->data[$name] = $value;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function has(string $name): bool {
        return array_key_exists($name, $this->data);
    }

    /**
     * @param string $name
     * @param string|null $default
     * @return string|null
     */
    public function get(string $name, string $default = null): string {
        return array_key_exists($name, $this->data) ? $this->data[$name] : $default;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return sizeof($this->data);
    }

    /**
     * @return array
     */
    public function getParamNames(): array {
        return array_keys($this->data);
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool {
        return (bool) $this->data;
    }

    public function clean() {
        $this->data = [];
    }
}