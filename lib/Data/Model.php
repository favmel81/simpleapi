<?php

namespace Data;


interface Model
{
    public static function createFromArray(array $data);

    public function getId();

    public function asArray(): array;
}