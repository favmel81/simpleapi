<?php

namespace Rest;

use Data\Params;
use Illuminate\Http\Request;

class Controller
{

    /**
     * @param Request $request
     * @param Params $params
     * @throws \Exception
     */
    public function run(Request $request, Params $params)
    {
        switch ($request->getMethod()) {
            case Request::METHOD_POST:
                return $this->post($request, $params);
                break;

            case Request::METHOD_GET:
                return $this->get($request, $params);
                break;

            case Request::METHOD_PUT:
                return $this->put($request, $params);
                break;

            case Request::METHOD_PATCH:
                return $this->patch($request, $params);
                break;

            default:
                return null;
        }
    }

    /**
     * @param Request $request
     * @param Params $params
     * @throws \Exception
     */
    protected function post(Request $request, Params $params)
    {
        $this->throwInvalidHandlerException(__METHOD__);
    }

    /**
     * @param Request $request
     * @param Params $params
     * @throws \Exception
     */
    protected function get(Request $request, Params $params)
    {
        $this->throwInvalidHandlerException(__METHOD__);
    }

    /**
     * @param Request $request
     * @param Params $params
     * @throws \Exception
     */
    protected function put(Request $request, Params $params)
    {
        $this->throwInvalidHandlerException(__METHOD__);
    }

    /**
     * @param Request $request
     * @param Params $params
     * @throws \Exception
     */
    protected function patch(Request $request, Params $params)
    {
        $this->throwInvalidHandlerException(__METHOD__);
    }

    /**
     * @param $method
     * @throws \Exception
     */
    private function throwInvalidHandlerException($method)
    {
        throw new \Exception('Method is absent: ' . $method);
    }
}