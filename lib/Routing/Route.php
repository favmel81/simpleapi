<?php

namespace Routing;

use Illuminate\Http\Request;

class Route
{
    /**
     * @var RouteMatcherInterface[]
     */
    private $matchers = [];

    /**
     * @var callable
     */
    private $handler;

    /**
     * @var string
     */
    private $prefix;

    /**
     * Route constructor.
     * @param $matchers
     * @param callable $handler
     * @param string $prefix
     * @throws \Exception
     */
    public function __construct($matchers, callable $handler, string $prefix = '')
    {
        if (!is_array($matchers)) {
            $matchers = [$matchers];
        }

        foreach ($matchers as $matcher) {
            if (!($matcher instanceof RouteMatcherInterface)) {
                throw new \Exception('Invalid matcher type !');
            }

            $this->addMatcher($matcher);
        }

        $this->handler = $handler;
        $this->prefix = $prefix;
    }

    /**
     * @param RouteMatcherInterface $matcher
     */
    public function addMatcher(RouteMatcherInterface $matcher)
    {
        $this->matchers[] = $matcher;
    }

    /**
     * @param $prefix
     */
    public function setPrefix($prefix) {
        $this->prefix = $prefix;
    }

    /**
     * @return string
     */
    public function getPrefix(): string {
        return $this->prefix;
    }

    /**
     * @param Request $request
     * @return RouteMatcherInterface|null
     */
    public function check(Request $request)
    {
        foreach ($this->matchers as $matcher) {
            if ($matcher->isMatch($request, $this)) {
                return $matcher;
            }
        }

        return null;
    }

    /**
     * @return callable
     */
    public function getHandler()
    {
        return $this->handler;
    }
}