<?php

namespace Routing;

use Illuminate\Http\Request;

class Router
{
    /**
     * @var Route[]
     */
    protected $routes = [];

    public function addRoute(Route $route) {
        $this->routes[] = $route;
    }

    /**
     * @param Request $request
     * @return RoutingResult
     */
    public function handleRequest(Request $request): RoutingResult {
        $matcher = null;
        $handler = null;

        foreach ($this->routes as $route) {
            $matcher = $route->check($request);
            if ($matcher) {
                $handler = $route->getHandler();
                break;
            }
        }

        return new RoutingResult($matcher, $handler);
    }
}