<?php

namespace Routing;


class RoutingResult
{

    /**
     * @var RouteMatcherInterface|null
     */
    private $routeMatcher;

    /**
     * @var callable|null
     */
    private $routeHandler;

    public function __construct(RouteMatcherInterface $matcher = null, callable $handler = null)
    {
        $this->routeMatcher = $matcher;
        $this->routeHandler = $handler;
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->routeMatcher ? true : false;
    }

    /**
     * @return RouteMatcherInterface|null
     */
    public function getRouteMatcher()
    {
        return $this->routeMatcher;
    }

    /**
     * @return callable
     */
    public function getRouteHandler(): callable
    {
        return $this->routeHandler;
    }
}