<?php

namespace Routing;

use Data\Params;
use Illuminate\Http\Request;

interface RouteMatcherInterface
{
    public function isMatch(Request $request, Route $route): bool;
    public function getParams(): Params;
}