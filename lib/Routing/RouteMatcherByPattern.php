<?php

namespace Routing;

use Illuminate\Http\Request;
use Data\Params;

class RouteMatcherByPattern implements RouteMatcherInterface
{

    /**
     * @var string
     */
    private $pattern;

    private $allowMethods = [
        Request::METHOD_GET,
        Request::METHOD_POST,
        Request::METHOD_DELETE,
        Request::METHOD_PATCH,
        Request::METHOD_PUT
    ];

    /**
     * @var array
     */
    private $methods;

    /**
     * @var Params
     */
    private $params;

    /**
     * RouteMatcherByPattern constructor.
     * @param string $pattern
     * @param null $methods
     */
    public function __construct(string $pattern, $methods = null)
    {
        $this->params = new Params();
        $this->pattern = $pattern;
        $this->methods = $this->allowMethods;
        $this->setMethods($methods);
    }

    public function setMethods($methods = null) {

        if (!is_array($methods)) {
            $methods = [$methods];
        }

        $newMethods = [];
        foreach ($methods as $method) {
            if (in_array($method , $this->allowMethods)) {
                $newMethods[] = $method;
            }
        }

        if ($newMethods) {
            $this->methods = $newMethods;
        }
    }

    public function addMethod($method) {
        if (in_array($method, $this->allowMethods) && !in_array($method, $this->methods)) {
            $this->methods[] = $method;
        }
    }

    private function getRegexp(&$params = [], $prefix = '') {


        // type1  /abc{?xyz}
        $type1 = '(?<type1>(?<type1prefix>[0-1a-z_]+){\?(?<type1param>[0-1a-z_]+)})';

        // type2  /abc{xyz}
        $type2 = '(?<type2>(?<type2prefix>[0-1a-z_]+){(?<type2param>[0-1a-z_]+)})';

        // type3 /{?xyz}
        $type3 = '(?<type3>{\?(?<type3param>[0-1a-z_]+)})';

        // type4 /{xyz}
        $type4 = '(?<type4>{(?<type4param>[0-1a-z_]+)})';

        $subPattern = '~^' . $type1 . '|' . $type2 . '|' . $type3 . '|' . $type4 . '$~';

        $regexp = '';

        $prefix = trim($prefix, '/ ');
        $prefix = explode('/', $prefix);
        foreach ($prefix as $chunk) {
            $chunk = trim($chunk);
            if ($chunk) {
                $regexp .= '/' . $chunk;
            }
        }

        $pattern = trim($this->pattern, '/ ');
        $pattern = explode('/', $pattern);

        foreach ($pattern as $chunk) {
            $chunk = trim($chunk);
            if (!$chunk) {
                continue;
            }

            if (preg_match($subPattern, $chunk, $matches)) {

                if (isset($matches['type1']) && $matches['type1']) {
                    $param = $matches['type1param'];
                    $params[] = $param;
                    $regexp .= "/{$matches['type1prefix']}(?<$param>[0-9a-z_]+)?";
                } elseif (isset($matches['type2']) && $matches['type2']) {
                    $param = $matches['type2param'];
                    $params[] = $param;
                    $regexp .= "/{$matches['type2prefix']}(?<$param>[0-9a-z_]+)";
                } elseif (isset($matches['type3']) && $matches['type3']) {
                    $param = $matches['type3param'];
                    $params[] = $param;
                    $regexp .= "(/(?<$param>[0-9a-z_]+))?";
                } elseif (isset($matches['type4']) && $matches['type4']) {
                    $param = $matches['type4param'];
                    $params[] = $param;
                    $regexp .= "/(?<$param>[0-9a-z_]+)";
                }
            } else {
                $regexp .= '/' . $chunk;
            }
        }

        return '~^' . trim($regexp, '/') . '$~i';
    }

    /**
     * @param Request $request
     * @param Route $route
     * @return bool
     */
    public function isMatch(Request $request, Route $route): bool {

        $this->params->clean();

        // Проверяем метод
        if (!in_array($request->getMethod(), $this->methods)) {
            return false;
        }

        $params = [];
        $uri = trim(explode('?', $request->getRequestUri())[0], '/ ');
        $pattern = $this->getRegexp($params, $route->getPrefix());

        if (preg_match($pattern, $uri, $matches)) {
            if ($params) {
                foreach ($params as $param) {
                    if (array_key_exists($param, $matches)) {
                        $this->params->add($param, $matches[$param]);
                    }
                }
            }

            return true;
        }

        return false;
    }

    /**
     * @return Params
     */
    public function getParams(): Params
    {
        return $this->params;
    }
}